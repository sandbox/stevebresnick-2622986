<?php

/**
 * @file
 * Contains \Drupal\tincanapi\Tests\TinCanController.
 */

namespace Drupal\tincanapi\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the tincanapi module.
 */
class TinCanControllerTest extends WebTestBase {
  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "tincanapi TinCanController's controller functionality",
      'description' => 'Test Unit for module tincanapi and controller TinCanController.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests tincanapi functionality.
   */
  public function testTinCanController() {
    // Check that the basic functions of module tincanapi.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via App Console.');
  }

}
