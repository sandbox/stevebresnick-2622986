<?php

/**
 * @file
 * Contains \Drupal\tincanapi\Form\AdminForm.
 */

namespace Drupal\tincanapi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AdminForm.
 *
 * @package Drupal\tincanapi\Form
 */
class AdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tincanapi_admin_form';
  }


  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tincanapi.admin'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('tincanapi.admin');

    $form['api'] = array(
      '#type' => 'fieldset',
      '#title' => t('API Settings'),
    );

    $form['api']['tincanapi_endpoint'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('The server endpoint. Do not include a trailing slash.'),
      '#maxlength' => 255,
      '#size' => 60,
      '#default_value' => $config->get('tincanapi_endpoint'),
    );
    $form['api']['tincanapi_auth_user'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('User'),
      '#description' => $this->t('The basic authenication user.'),
      '#maxlength' => 255,
      '#size' => 60,
      '#default_value' => $config->get('tincanapi_auth_user'),
    );
    $form['api']['tincanapi_auth_password'] = array(
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('The basic authenication password.'),
      '#maxlength' => 255,
      '#size' => 60,
      '#default_value' => $config->get('tincanapi_auth_password'),
    );
    $form['api']['tincanapi_anonymous'] = array(
      '#type' => 'checkbox',
      '#title' => t('Track anonymous users.'),
      '#default_value' => $config->get('tincanapi_anonymous'),
    );

    $form['api']['tincanapi_watchdog'] = array(
      '#type' => 'checkbox',
      '#title' => t('Log server response.'),
      '#default_value' => $config->get('tincanapi_watchdog'),
    );

    $form['api']['tincanapi_simplify_id'] = array(
      '#type' => 'checkbox',
      '#title' => t('Simplify statements IDs (convert https urls to http)'),
      '#default_value' => $config->get('tincanapi_simplify_id'),
    );

    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('token')){
      $form['statements'] = array(
        '#type' => 'fieldset',
        '#title' => t('Statement Settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      $form['statements']['tincanapi_statement_actor'] = array(
        '#type' => 'textfield',
        '#title' => t('Statement Actor'),
        '#description' => t('The token replacements for the actor name in a statement.'),
        '#default_value' => $config->get('tincanapi_statement_actor'),
      );

      $form['statements']['token_help'] = array(
        '#theme' => 'token_tree_link',
        '#token_types' => array('user'),
      );
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('tincanapi.admin');
    $config->set('tincanapi_endpoint', $form_state->getValue(array('api', 'tincanapi_endpoint'))
      ->save());
    $config->set('tincanapi_auth_user', $form_state->getValue(array('api', 'tincanapi_auth_user'))
      ->save());
    $config->set('tincanapi_auth_password', $form_state->getValue(array('api', 'tincanapi_auth_password'))
      ->save());
    $config->set('tincanapi_anonymous', $form_state->getValue(array('api', 'tincanapi_anonymous'))
      ->save());
    $config->set('tincanapi_watchdog', $form_state->getValue(array('api', 'tincanapi_watchdog'))
      ->save());
    $config->set('tincanapi_simplify_id', $form_state->getValue(array('api', 'tincanapi_simplify_id'))
      ->save());
    $config->set('tincanapi_statement_actor', $form_state->getValue(array('statements', 'tincanapi_statement_actor'))
      ->save());



    parent::submitForm($form, $form_state);
  }
}
