<?php

/**
 * @file
 * Contains \Drupal\tincanapi\Controller\TinCanController.
 */

namespace Drupal\tincanapi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity;

/**
 * Class TinCanController.
 *
 * @package Drupal\tincanapi\Controller
 */
class TinCanController extends ControllerBase {
  /**
   * Configure.
   *
   * @return string
   *   Return Hello string.
   */
  public function configure() {
    return [
        '#type' => 'markup',
        '#markup' => $this->t('Implement method: configure')
    ];
  }

  function tincanapi_send($action, $method, array $data) {
    $end_point = \Drupal::config()->get('tincanapi_endpoint');
    $basic_auth_user = \Drupal::config()->get('tincanapi_auth_user');
    $basic_auth_pass = \Drupal::config()->get('tincanapi_auth_password');

    // Sanitize endpoint.
    $end_point = trim($end_point);
    if (substr($end_point, -1) == "/") {
      $end_point = substr($end_point, 0, -1);
    }

    // Sanitize action.
    $action = trim($action);
    if (substr($end_point, 0, 1) == "/") {
      $action = substr($end_point, 1);
    }

    if ($method == "GET") {
      $action .= "?" . http_build_query($data);
    }

    // Init call.
    $url = $end_point . '/' . $action;
    $ch = curl_init($url);

    $headers = array(
      'Content-Type: application/json',
      'X-Experience-API-Version: 1.0.0',
    );

    // Differentiate for different methods.
    if ($method == "POST") {
      $json = json_encode($data);
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
      $headers[] = 'Content-Length: ' . strlen($json);
    }

    // Make CURL request.
    curl_setopt($ch, CURLOPT_USERPWD, $basic_auth_user . ':' . $basic_auth_pass);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $response = curl_exec($ch);

    // Check for connection errors.
    if ($response === FALSE || $response === NULL) {
      if (\Drupal::config()->get('tincanapi_watchdog')) {
        \Drupal::logger('tincanapi')->error($ch);
      }

      return;
    }

    $response = json_decode($response, TRUE);
    $error = isset($response["error"]) && $response["error"];

    // Log the response.
    if (\Drupal::config()->get('tincanapi_watchdog') || $error) {
      $debug = array(
        "request" => array(
          "url" => $url,
          "post" => $data,
        ),
        "response" => array(
          "txt" => $response,
        ),
      );

      \Drupal::logger('tincanapi')->error(json_encode($debug));
    }
  }


  /**
   * Send the data collection to the LRS.
   *
   * @param string $module_name
   *   The name of the sub module that is tracking data.
   * @param array $data
   *   An associative array that conforms to the API specification.
   * @param mixed $user
   *   Optional. A user object or user id.
   */
  function tincanapi_track_data($module_name, $data = array(), $user = NULL) {
    if ((\Drupal::currentUser()->isAnonymous()) && !\Drupal::config()->get('tincanapi_anonymous')) {
      return;
    }

    $data['actor'] = tincanapi_get_actor($user);
    $data['storage'] = isset($data['storage']) ? $data['storage'] : array();

    $hook = 'tincanapi_data_alter';

    foreach (\Drupal::moduleHandler()->getImplementations($hook) as $module) {
      $function = $module . '_' . $hook;
      $function($module_name, $data);
    }

    unset($data['storage']);

    if (!empty($data)) {

      // Simplify the data IDs to http version.
      if (\Drupal::config()->get('tincanapi_simplify_id')) {
        if (isset($data["object"]) && isset($data["object"]["id"])) {
          $data["object"]["id"] = str_replace(array("http://www.", "https://www.", "https://", "https://"), "http://", $data["object"]["id"]);
        }
        if (isset($data["context"]) && isset($data["context"]["contextActivities"]) && isset($data["context"]["contextActivities"]["grouping"]) && isset($data["context"]["contextActivities"]["grouping"][0]) && isset($data["context"]["contextActivities"]["grouping"][0]["id"])) {
          $data["context"]["contextActivities"]["grouping"][0]["id"] = str_replace(array("http://www.", "https://www.", "https://", "https://"), "http://", $data["context"]["contextActivities"]["grouping"][0]["id"]);
        }
        if (isset($data["context"]) && isset($data["context"]["contextActivities"]) && isset($data["context"]["contextActivities"]["parent"]) && isset($data["context"]["contextActivities"]["parent"][0]) && isset($data["context"]["contextActivities"]["parent"][0]["id"])) {
          $data["context"]["contextActivities"]["parent"][0]["id"] = str_replace(array("http://www.", "https://www.", "https://", "https://"), "http://", $data["context"]["contextActivities"]["parent"][0]["id"]);
        }
      }

      $this->tincanapi_send("statements", "POST", $data);
    }
  }

  /**
   * Menu callback for JavaScript event tracking.
   */
  public function tincanapi_ajax_track() {

    if (isset($_POST['token']) && \Drupal::csrfToken()->get() == $_POST['token'] && isset($_POST['module'])) {
      $module_name = $_POST['module'];
      $context = $_POST;

      $data = array();
      $data['storage'] = array();

      $hook = 'tincanapi_ajax_data_alter';

      foreach (\Drupal::moduleHandler()->getImplementations($hook) as $module) {
        $function = $module . '_' . $hook;
        $function($module_name, $data, $context);
      }

      // Check to see if the data array is empty without the storage array.
      $storage = $data['storage'];
      unset($data['storage']);

      if (!empty($data)) {
        $data['storage'] = $storage;
        $this->tincanapi_track_data($module_name, $data);
      }
    }
  }

}
