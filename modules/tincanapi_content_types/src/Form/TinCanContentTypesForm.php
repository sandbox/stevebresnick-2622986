<?php

/**
 * @file
 * Contains \Drupal\tincanapi_content_types\Form\TinCanContentTypesForm.
 */

namespace Drupal\tincanapi_content_types\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeRepository;

/**
 * Class TinCanContentTypesForm.
 *
 * @package Drupal\tincanapi_content_types\Form
 */
class TinCanContentTypesForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tincanapi_content_types.tincancontenttypes'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tincanapi_content_types_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tincanapi_content_types.tincancontenttypes');

    //Get all of the available content types and put them in an array

   $form['track_content_type_views'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Track content type views'),
      '#description' => $this->t(''),
      '#options' => array(0 => $this->t('Blog entry'), 1 => $this->t('Poll'), 2 => $this->t('Forum topic'), 3 => $this->t('Article'), 4 => $this->t('Book page')),
      '#default_value' => $config->get('track_content_type_views'),
    );
    $form['track_content_types_view_modes'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Track the selected content types in the following view modes.'),
      '#options' => array(0 => $this->t('Full content'), 1 => $this->t('Teaser'), 2 => $this->t('RSS'), 3 => $this->t('Search index'), 4 => $this->t('Search result highlighting input')),
      '#default_value' => $config->get('track_content_types_view_modes'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('tincanapi_content_types.tincancontenttypes')
      ->set('track_content_type_views', $form_state->getValue('track_content_type_views'))
      ->set('track_content_types_view_modes', $form_state->getValue('track_content_types_view_modes'))
      ->save();
  }

}
